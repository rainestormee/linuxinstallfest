# Install instructions

In the following sections you will find info on how to install Linux. You've chosen your
install method, now let's talk a bit about Linux distributions. You should choose one,
but if this is your first encounter with Linux, we recommend you go for Ubuntu. 

Linux comes in different flavours or "Distributions" (aka distros). While it's the same *kernel* (core of an operating system) they can each work differently on top e.g how software gets installed, how networking works, how it looks. There are endless distros out there if you want, but to make things simpler we've chosen to write this guide for **2**. These are:

## Ubuntu

![Ubuntu](images/ubuntu.jpeg)

Maybe the most popular distribution out there, Ubuntu is a really great distro. It looks great and is easy to use, it's kept up to date and has great support for packages and software.
Just because it's "good for beginners" doesn't mean it's any less advanced / capable than any other distro, remember they're all built on the same underlying kernel, it's just *how* things get done on top of that that changes.

## Manjaro

![Manjaro](images/manjaro.png)

Manjaro is an open source distro built around Arch Linux: a distribution that focuses on a DIY attitude meaning you get to fine tune every bit of how it works. However Manjaro is basically "Arch that works out of the box", so is completely beginner friendly. Read more about it here: [manjaro.org](https://manjaro.org/)

