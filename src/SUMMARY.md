# Summary

- [Welcome](./welcome.md)

---
- [README before anything else!](./disclaimer.md)
- [Install methods](./methods.md)
- [Dualboot pre-install checks](./dualboot-preinstall.md)
- [Virtual Machine pre-install](./vm-preinstall.md)
- [Install](./install.md)
    - [Ubuntu](./install/ubuntu.md)
      - [Just Ubuntu](./install/ubuntu/pure.md)
      - [Dualboot](./install/ubuntu/dualboot.md)
      - [VM](./install/ubuntu/vm.md)
      - [WSL](./install/ubuntu/wsl.md)
    - [Manjaro](./install/manjaro.md)
      - [Just Manjaro](./install/manjaro/pure.md)
      - [Dualboot](./install/manjaro/dualboot.md)
      - [VM](./install/manjaro/vm.md)
- [Post-install](./post-install.md)
  - [Chromium](./post-install/chromium.md)
  - [Connecting to Eduroam](./post-install/eduroam.md)
  - [Java](./post-install/java.md)
  - [Neofetch](./post-install/neofetch.md)
  - [OS-Prober](./post-install/os-prober.md)
  - [Speedtest](./post-install/speedtest.md)
  - [UFW](./post-install/ufw.md)
- [Post-install for VMs](./vm-post-install-ga.md)
- [Troubleshooting](./troubleshooting.md)

---

- [Learning](./learning.md)
