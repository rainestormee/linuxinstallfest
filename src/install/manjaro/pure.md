# Replacing current OS with Manjaro

<span style="color:red; font-weight:bold">Warning: This method will delete your Windows partition. Please make sure
you have made a backup of all the data you do not want to loose.</span>

## Step 1 (Preparation)

Before starting, [download Manjaro](https://manjaro.org/downloads/official/gnome/).

You'll get an ISO file, which is a disk image. You need to flash this to a
USB drive, using an imager tool. The Raspberry Pi foundation keeps an
up-to-date list of [imagers for Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md),
however, we recommend [Etcher](https://www.balena.io/etcher/). [Rufus](https://rufus.ie/) is another good ISO burner.


In Etcher:
- Select your ISO file
- Select your USB drive (plug it in!)
- Then flash!

![Etcher](../../images/manjaro/etcher.png)

## Step 2 (Boot)

To boot into the live installation media, plug your USB into your
computer/laptop, and reboot - you should be able to boot off it with no
problems.

Once boot has finished, you should be presented with the Manjaro Hello utility!

![Installer](../../images/manjaro/manjaro_hello.png)

Close it and play around with the system. Make sure everything works (keyboard, trackpad).
You can also connect to the wifi:

![Wifi](../../images/manjaro/wifi.png)

And choose the network:

![Network](../../images/manjaro/choose_network.png)

## Step 3 (Installation)

Once you're happy everything works okay, you can proceed to install it. Click the
disk w/ a green arrow on the left hand side bar. This will start the installer.
Choose the language:

![Language](../../images/manjaro/installer_language.png)

Your region:

![Installer Region](../../images/manjaro/installer_region.png)

And your keyboard layout:

![Installer Keyboard](../../images/manjaro/installer_keyboard.png)

Next, we will take care of partitioning. Choose Erase disk and choose Swap (with Hibernate). You
may also choose to tick the box to encrypt the system. Leave any other settings
as they are.

**Note: if you have enough RAM and an SSD, you can go with no swap. However, you may 
not be able to put your laptop in hibernate mode without more tinkering. If you are low
on RAM, you should definitely create a swap partition. Part of the role of the swap partition is to
hold files which should be in RAM, but there is not enough memory to hold them all.


![../../images/manjaro/partitions.png](../../images/manjaro/manjaro_with_swap.png)

Now we'll create the main user. Fill in your name, the name you want to use
to login (this will be your **username**) and a name for the machine (this
will be the name the VM will be seen as over a network). Next, choose your
account password. Make it a good one! You can then also choose the
administrator account password (which is the `root` user), or choose to 
use the first supplied password for the admin account too.

![../../images/manjaro/users.png](../../images/manjaro/users.png)

You can then choose to install LibreOffice or FreeOffice (the Linux
alternatives to Microsoft Office).

When you're ready, press Install! You will be prompted that disk changes
will be made in order to install Manjaro. Confirm by pressing Install now.
Wait for the installation to finish.

Once it's completed, follow the prompts to shutdown, remove the installation
media, and restart your computer. When you startup, you should be booted into
Manjaro!

And remember, the most important think about using Arch is to tell people as 
often as possible 

`btw, I use Arch` &#128516;

Now head over to the [Post Installation](../../post-install.md) guide to update your
system and install some useful software.