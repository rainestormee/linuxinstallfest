# Connecting to Eduroam

This guide will help you get the eduroam connection working.
It assumes you are using Network Manager for handing your network connections
(which if you've been following our install guides, should be true).

You will only need `wget` as extra software. Install it:

Ubuntu:

    sudo apt install wget

Manjaro:

    sudo pacman -S wget

## Certificate

From the terminal:

Make a folder in your home directory (I'll use the folder `.eduroam_ca` in this guide), download the file:

    mkdir ~/.eduroam_ca
    cd ~/.eduroam_ca
    wget https://linux.afnom.net/files/ca.pem

Now download the connection configuration:

    wget https://linux.afnom.net/files/eduroam.nmconnection

**You have to edit this file**. Open it with gedit:

    gedit eduroam.nmconnection &

* Replace `[computer user]` (the whole thing, `[]` included!) with **your** username. You 
can find it out by running `whoami` in the terminal.
* Replace `[path to ca.pem]` with the **full path** to the `ca.pem` file.
The full path should look something like `/home/[computer user]/.eduroam_ca/ca.pem` 
where `[computer user]` is **your** username (like I explained above).
* Replace `[student username]` with **your** UoB student username. They look
something like `abc123`
* Replace `[UoB password]` with **your** UoB account password.

Save and close the window.

Copy the file in the configurations directory of Network Manager (you **must** use sudo):

    sudo cp eduroam.nmconnection /etc/NetworkManager/system-connections

Reload Network Manager:

    sudo systemctl restart NetworkManager

You should now be able to see `eduroam` in your WiFi list and, when in range, connect to it.