# Manjaro install

Manjaro is "an accessible, friendly, open-source operating system", that is
based off of ArchLinux.

The following section will give you the instructions on how to install Manjaro 
on your computer.

Please choose the appropriate method of installation.

<span style="color:red; font-weight:bold">Warning!</span>
If you are installing it as a replacement OS or **dualboot**, please make sure
you have a **backup** of any data you do not want to lose. We are providing the
instructions as they are, and **we are not responsible for any data loss that
might occur. Use at your own risk**.


## Choose from:

- [Just Manjaro](./manjaro/pure.md) -- replace your current OS with
  Manjaro; this will **completely** erase data on your computer; **backup**
  anything you need. 
- [Dualboot](./manjaro/dualboot.md) -- install Manjaro alongside an
  existing Windows install; **backup** any important data, in case something
  goes wrong 
- [VM](./manjaro/vm.md) -- install Manjaro in a Virtual Machine; there
  will be no data loss; instructions are for Windows 
