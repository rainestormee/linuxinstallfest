# Chromium
Chromium is an open-source web browser.

## Installation
Manjaro:

     sudo pacman -S chromium

Ubuntu:

    sudo apt install chromium-browser

## Running
You can either run Chromium by searching it in the applications menu (by pressing the Super (Windows) key and typing `Chromium`), or by running the following command:

    chromium

## Removal
Manjaro:

     sudo pacman -Rs chromium

Ubuntu:

    sudo apt remove chromium-browser
